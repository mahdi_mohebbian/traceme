import folium
import requests
import ast
import numpy as np
def clearmap(map):
    for i in map._layers:
        if map._layers[i]._path != None:
            try:
                map.removeLayer(map._layers[i])
            except :
                pass

url = "http://3.134.85.176:8000/api/users/"

r = requests.get(url = url) 
  
# extracting data in json format 
data = r.json() 
print("users info : \n")
user_id_list = list()
for user in data :
    if user["is_active"] == True:
        active = "Online"
    else :
        active = "Offline"
    
    user_id_list.append(user["id"])
    print("id: {} - name: {} - status: {}".format(user["id"],user["name"],active))
    print("\n")

print("\n\n")
print("Enter a user's id for tracking: \n")
id = input()
length = int(input("Enter data length : "))
if int(id) not in user_id_list:
    print(id)
    print(user_id_list)
    print ("Wrong ID.")
    exit

m = folium.Map(location=[58.37, 26.72],
              tiles="Stamen Toner",
              zoom_start = 13)

data = {"id":id, "length":15}
url = "http://3.134.85.176:8000/api/trace/"
r = requests.post(url = url, data = data)

pos_data = ast.literal_eval(r.text)


transparent_num = np.linspace(start=0, stop=1, num=len(pos_data))
index=len(pos_data)-1
places = list()
for pos in pos_data:
    clr = ""
    if pos["mean_of_travel"] == "car":
        clr="blue"

    if pos["mean_of_travel"] == "on_foot":
        clr="red"

    if pos["mean_of_travel"] == "bus":
        clr="green"

    if pos["mean_of_travel"] == "subway":
        clr="yellow"
    places.append((float(pos["lat"]),float(pos["lon"])))
    folium.Circle([float(pos["lat"]),float(pos["lon"])],
                        radius=30,
                      color=clr,
                      fill=True,
                      fill_color=clr,
                      opacity=transparent_num[index]
                       ).add_to(m)
    
    index -= 1
folium.PolyLine(places).add_to(m)
m.save(outfile="result.html")
del(m)

print("refresh the result.html webpage!")
print("-----------------")

print("| RED : ON FOOT")
print("| GREEN : BUS")
print("| BLUE : CAR")
print("| YELLOW : SUBWAY")
print(("-----------------"))