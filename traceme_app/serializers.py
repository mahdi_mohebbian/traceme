from rest_framework import serializers
from . import models


class UserProfile_serializer(serializers.ModelSerializer):
    class Meta:
        model = models.UserProfile
        fields = "__all__"

class Pos_rec_serializer(serializers.ModelSerializer):
    class Meta:
        model = models.Pos_rec
        fields = "__all__"