import datetime
from . import models
from rest_framework.response import Response
from rest_framework import status
from rest_framework.authtoken.models import Token
from rest_framework import parsers, permissions
from rest_framework.views import APIView
from django.contrib.auth import hashers
from . import serializers
import requests
import json
import time
# Create your views here.

class LoginAPIView(APIView):
    permission_classes = (permissions.AllowAny,)
    def get(self,request):
        pass

    def post(self,request):
        email = request.data["email"]
        password = request.data["password"]

        try :
            user = models.UserProfile.objects.get(email=email)
        except :
            user = None

        if user==None:
            return Response({"message":"wrong email address or password"},status=status.HTTP_400_BAD_REQUEST)

        if hashers.check_password(password,user.password)==True :
            try:
                token = Token.objects.get(user=user)
            except:
                token = None

            if token == None :
                token = Token.objects.create(user=user)
            else :
                token.delete()
                token = Token.objects.create(user=user)

            
            return Response({'Token':token.__str__()},status=status.HTTP_200_OK)

        
        return Response({"message":"wrong email address or password"},status=status.HTTP_400_BAD_REQUEST)


class User_registration_APiView(APIView):
    permission_classes = (permissions.AllowAny,)
    def post(self,request):
        try :
            email = request.data['email']
            phone_number = request.data['phone_number']
            name = request.data['name']
            password = request.data['password']

            new_user = models.UserProfile(
                email = email,
                phone_number= phone_number,
                name = name,
                password= hashers.make_password(password)

            )
            new_user.save()
            return Response({"message":"user has been registered successfully !"},status=status.HTTP_201_CREATED)
        except:
            return Response({"message":"incomplete user information"},status=status.HTTP_400_BAD_REQUEST)


class SetData(APIView):
    def get(self,request):
        pass

    def post(self,request):
        user_id = request.user.id
        lat = request.data["lat"]
        lon = request.data["lon"]
        mean = request.data["mean"]


        record = models.Pos_rec(user=request.user,lat=float(lat),lon=float(lon),mean_of_travel=mean)
        record.save()
        return Response({"Message":"data stored"},status=status.HTTP_201_CREATED)




class TraceUser(APIView):
    permission_classes = (permissions.AllowAny,)
    def get(self,request):

        pass

    def post(self,request):

        user_id = request.data["id"]
        data_length = int(request.data["length"])

        user = models.UserProfile.objects.get(id=int(user_id))
        trace_data = models.Pos_rec.objects.filter(user=user).order_by('-date','-time')
        if data_length < len(trace_data):
            trace_data = trace_data[0:data_length]
        serializer = serializers.Pos_rec_serializer(trace_data,many=True)

        return Response(serializer.data,status.HTTP_200_OK)

#====
import math
def DistanceMeasure(x1,y1,x2,y2):
    return math.sqrt( ((x1-x2)*(x1-x2)) + ((y1-y2)*(y1-y2)) )
#====


class StatitcsData(APIView):
    def get(self, request):

        trans_data = models.Pos_rec.objects.filter(user=request.user)
        car=0
        walk=0
        bus=0
        subway=0

        #====
        n_lat = 58.3790
        n_lon = 26.7261
        e_lat = 58.3687
        e_lon = 26.7596
        w_lat = 58.3680
        w_lon = 26.6941
        s_lat = 58.252004
        s_lon = 26.7248


        nt=0
        et=0
        st=0
        wt=0
        #====
        for item in trans_data:

            n_d = DistanceMeasure(item.lat, item.lon, n_lat, n_lon)
            e_d = DistanceMeasure(item.lat, item.lon, e_lat, e_lon)
            w_d = DistanceMeasure(item.lat, item.lon, w_lat, w_lon)
            s_d = DistanceMeasure(item.lat, item.lon, s_lat, s_lon)

            lst = [n_d, e_d, w_d, s_d]
            m = min(lst)
            if m == n_d:
                nt += 1
            if m == s_d:
                st += 1

            if m == e_d:
                et += 1

            if m == w_d:
                wt += 1

            if item.mean_of_travel == "car":
                car += 1
            elif item.mean_of_travel == "bus":
                bus += 1
            elif item.mean_of_travel == "on_foot":
                walk += 1
            else:
                subway += 1

        # for item in trans_data:
        #     n_d = DistanceMeasure(item.lat,item.lon,n_lat,n_lon)
        #     e_d = DistanceMeasure(item.lat, item.lon, e_lat, e_lon)
        #     w_d = DistanceMeasure(item.lat, item.lon, w_lat, w_lon)
        #     s_d = DistanceMeasure(item.lat, item.lon, s_lat, s_lon)
        #
        #     lst = [n_d,e_d,w_d,s_d]
        #     m = min(lst)
        #     if m == n_d:
        #         nt+=1
        #     if m == s_d:
        #         st+=1
        #
        #     if m == e_d:
        #         et+=1
        #
        #     if m == w_d:
        #         wt+=1
        #
        #
        #     if item.mean_of_travel == "car":
        #         car += 1
        #     elif item.mean_of_travel == "bus":
        #         bus += 1
        #     elif item.mean_of_travel == "on_foot":
        #         walk += 1
        #     else :
        #         subway += 1

        all = car+walk+bus+subway
        all_tartu = nt+et+st+wt

        response_data = dict()
        response_data["car"] = (100*car/all).__str__()+"%"
        response_data["on_foot"] = (100*walk / all).__str__()+"%"
        response_data["bus"] = (100*bus / all).__str__()+"%"
        response_data["subway"] = (100*subway / all).__str__()+"%"

        response_data["north_tartu"] = (100 * nt / all_tartu).__str__() + "%"
        response_data["east_tartu"] = (100 * et / all_tartu).__str__() + "%"
        response_data["south_tartu"] = (100 * st / all_tartu).__str__() + "%"
        response_data["west_tartu"] = (100 * wt / all_tartu).__str__() + "%"

        return Response(response_data,status.HTTP_200_OK)



    def post(self, request):
        pass

class AllUsers(APIView):
    permission_classes = (permissions.AllowAny,)
    def get(self,request):
        users = models.UserProfile.objects.all()
        now = datetime.datetime.now()
        for user in users :
            data = models.Pos_rec.objects.filter(user=user).order_by('-date','-time')
            if len(data)==0:
                user.is_active=False
                continue
            data = data[0]
            user_date = datetime.datetime(data.date.year,data.date.month,data.date.day,
                                          data.time.hour,data.time.minute,data.time.second)
            print(now)
            print(user_date)
            if (now-user_date)<datetime.timedelta(minutes=5):

                user.is_active=True
            else :
                user.is_active=False

        serials = serializers.UserProfile_serializer(users,many=True)
        return Response(serials.data,status.HTTP_200_OK)