from django.apps import AppConfig


class TracemeAppConfig(AppConfig):
    name = 'traceme_app'
