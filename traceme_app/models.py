from django.db import models

import os

import django.utils.timezone as django_date
from django.contrib.auth.base_user import BaseUserManager, AbstractBaseUser
from django.contrib.auth.models import PermissionsMixin
from django.db import models
from django.dispatch import receiver
import datetime


class UserProfileManager(BaseUserManager):

    def create_user(self,email,name,phone_number,password=None):

        email = self.normalize_email(email)
        user = self.model(email=email,name=name,phone_number=phone_number,password=password)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, email, name, phone_number, password=None):
        user = self.create_user(email=email,name=name,phone_number=phone_number,password=password)
        user.is_staff = True
        user.is_superuser = True
        user.save(using=self._db)




class UserProfile(AbstractBaseUser,PermissionsMixin):
    name = models.CharField(max_length=100)
    phone_number = models.BigIntegerField(unique=True)
    email = models.EmailField(unique=True)
    password = models.CharField(max_length=30,default="12345")
    created_on = models.DateField(auto_now=True)
    is_active = models.BooleanField(default=True)
    is_staff = models.BooleanField(default=False)
    objects = UserProfileManager()

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['phone_number', 'name']

    def get_short_name(self):
        return self.name

    def get_full_name(self):
        return self.name

    def __str__(self):
        return self.id.__str__()+" : "+self.name



class Pos_rec(models.Model):
    user = models.ForeignKey('UserProfile',models.CASCADE,null=False)
    lat = models.FloatField(default=-1.0)
    lon = models.FloatField(default=-1.0)

    #bus
    #car
    #on_foot
    #subway
    mean_of_travel = models.CharField(max_length=30)
    date = models.DateField(auto_now=True)
    time = models.TimeField(auto_now=True)

    def __str__(self):
        return "{} :{}--{} ___ {}-{}-{}".format(self.user.__str__(),self.date.__str__(),self.time.__str__(),self.lat,self.lon,self.mean_of_travel)




###### Authentication stuff for admin users 

# class AdminUsers(models.Model):
#     name = models.CharField(max_length=50)
#     email = models.CharField(max_length=60)
#     password = models.CharField(max_length=30,default="12345")

#     def __str__(self):
#         return self.name

